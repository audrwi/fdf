# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/04 15:30:49 by aboucher          #+#    #+#              #
#    Updated: 2016/01/13 13:31:42 by aboucher         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf

SRC = main.c	\
	  draw.c	\
	  actions.c	\
	  bonus.c

OBJ = $(SRC:.c=.o)

HEAD = fdf.h

FLAGS = -Wall -Werror -Wextra

MLX = -L/usr/local/lib/ -I/usr/local/include	\
	  -lmlx -framework OpenGL -framework AppKit

all: $(NAME)

$(NAME): $(OBJ) makelibft
	@clang $(FLAGS) -c $(SRC) -I$(HEAD)
	@clang $(OBJ) libft/libft.a $(MLX) -o $(NAME)
	@echo "\033[0;36m"
	@echo "****************************"
	@echo "*      __________  ______  *"
	@echo "*     / ____/ __ \/ ____/  *"
	@echo "*    / /_  / / / / /_      *"
	@echo "*   / __/ / /_/ / __/      *"
	@echo "*  /_/   /_____/_/         *"
	@echo "*                          *"
	@echo "****************************"
	@echo "\033[0m"
	@echo "Compilation success."

makelibft:
	@make -C libft/
	@echo "Library included."

clean:
	@rm -f $(OBJ)
	@make -C libft/ clean
	@echo "Objects cleaned."

fclean: clean
	@rm -f $(NAME)
	@make -C libft/ fclean
	@echo "Target cleaned."

re: fclean all

.PHONY: all makelibft clean fclean re
