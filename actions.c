/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   actions.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 13:54:11 by aboucher          #+#    #+#             */
/*   Updated: 2016/01/14 17:48:10 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int				key_actions(int keycode, t_env *e)
{
	if (keycode == 53)
	{
		mlx_destroy_image(e->mlx, e->img);
		mlx_destroy_window(e->mlx, e->win);
		exit(0);
	}
	else if (keycode == 69 || keycode == 78 || keycode == 24 || keycode == 27)
		zoom(keycode, e);
	else if (keycode > 122 && keycode < 127)
		move(keycode, e);
	else if (keycode == 67 || keycode == 75 || keycode == 28 || keycode == 44)
		change_z(keycode, e);
	else if (keycode == 49)
		init(e);
	return (0);
}

void			zoom(int keycode, t_env *e)
{
	if (keycode == 69 || keycode == 24)
	{
		e->scale += 2;
		e->left -= 20;
	}
	else if ((keycode == 78 || keycode == 27) && e->scale - 2 >= 0)
	{
		e->scale -= 2;
		e->left += 20;
	}
	new_image(e);
}

void			move(int keycode, t_env *e)
{
	if (keycode == 123)
		e->left += 50;
	else if (keycode == 124)
		e->left -= 50;
	else if (keycode == 125)
		e->top -= 50;
	else if (keycode == 126)
		e->top += 50;
	new_image(e);
}

void			change_z(int keycode, t_env *e)
{
	t_matrix	*m;

	m = e->m;
	if (keycode == 67 || keycode == 28)
	{
		while (m->next != e->m)
		{
			m->z *= m->orig != 0 && m->z != 0 ? 2 : 1;
			m = m->next;
		}
		m->z *= m->orig != 0 && m->z != 0 ? 2 : 1;
	}
	else if (keycode == 75 || keycode == 44)
	{
		while (m->next != e->m)
		{
			m->z /= m->orig != 0 && ft_abs(m->z) > ft_abs(m->orig) ? 2 : 1;
			m = m->next;
		}
		m->z /= m->orig != 0 && ft_abs(m->z) > ft_abs(m->orig) ? 2 : 1;
	}
	new_image(e);
}

void			init(t_env *e)
{
	t_matrix	*m;

	m = e->m->prev;
	e->scale = m->x > 200 || m->y > 200 ? 0.5 : 20;
	e->left = 100;
	e->top = 400;
	m = m->next;
	while (m->next != e->m)
	{
		m->z = m->orig;
		m = m->next;
	}
	m->z = m->orig;
	new_image(e);
}
