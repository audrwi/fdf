/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bonus.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 13:29:41 by aboucher          #+#    #+#             */
/*   Updated: 2016/01/22 16:11:04 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			legends(t_env *e)
{
	int			x;
	int			y;
	int			c;

	x = 0;
	c = 0xFFFFFF;
	while (x <= MAX_WIDTH)
	{
		y = MIN_HEIGHT;
		while (y <= MAX_WIDTH)
		{
			mlx_pixel_put(e->mlx, e->win, x, y, 0x22313F);
			y++;
		}
		x++;
	}
	key_legend(e, c);
}

void			key_legend(t_env *e, int c)
{
	print_key(e, "| * |", 25, MIN_HEIGHT + 5);
	mlx_string_put(e->mlx, e->win, 85, MIN_HEIGHT + 20, c, "increase z");
	print_key(e, "| / |", 25, MIN_HEIGHT + 35);
	mlx_string_put(e->mlx, e->win, 85, MIN_HEIGHT + 50, c, "decrease z");
	print_key(e, "| + |", 225, MIN_HEIGHT + 5);
	mlx_string_put(e->mlx, e->win, 285, MIN_HEIGHT + 20, c, "zoom in");
	print_key(e, "| - |", 225, MIN_HEIGHT + 35);
	mlx_string_put(e->mlx, e->win, 285, MIN_HEIGHT + 50, c, "zoom out");
	print_key(e, "| ^ |", 455, MIN_HEIGHT + 5);
	print_key(e, "| < |", 405, MIN_HEIGHT + 35);
	print_key(e, "| v |", 455, MIN_HEIGHT + 35);
	print_key(e, "| > |", 505, MIN_HEIGHT + 35);
	mlx_string_put(e->mlx, e->win, 595, MIN_HEIGHT + 10, c, "------------");
	mlx_string_put(e->mlx, e->win, 590, MIN_HEIGHT + 20, c, "|   space   |");
	mlx_string_put(e->mlx, e->win, 595, MIN_HEIGHT + 32, c, "------------");
	mlx_string_put(e->mlx, e->win, 615, MIN_HEIGHT + 50, c, "init map");
}

void			print_key(t_env *e, char *str, int x, int y)
{
	mlx_string_put(e->mlx, e->win, x + 5, y + 5, 0xFFFFFF, "----");
	mlx_string_put(e->mlx, e->win, x, y + 15, 0xFFFFFF, str);
	mlx_string_put(e->mlx, e->win, x + 5, y + 27, 0xFFFFFF, "----");
}

void			new_image(t_env *e)
{
	mlx_destroy_image(e->mlx, e->img);
	e->img = mlx_new_image(e->mlx, MIN_WIDTH, MIN_HEIGHT);
	e->data = mlx_get_data_addr(e->img, &e->bpp, &e->size, &e->endian);
	add_points(e);
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
}

int				norminator(t_env *e, t_matrix *m)
{
	if (m == m->next)
	{
		m = calc_xy(e, m);
		pixel_put_to_image(0xFFFFFF, e, m->cx, m->cy);
	}
	return (0);
}
