/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 14:56:54 by aboucher          #+#    #+#             */
/*   Updated: 2016/01/22 16:11:12 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			draw_line(t_env *e, int x1, int y1, t_matrix *p2)
{
	int			x;
	int			y;
	int			err;
	int			e2;

	x = ft_abs(p2->cx - x1);
	y = ft_abs(p2->cy - y1);
	err = (x > y ? x : -y) / 2;
	while (x1 != p2->cx || y1 != p2->cy)
	{
		pixel_put_to_image(0xFFFFFF, e, x1, y1);
		e2 = err;
		if (e2 > -x)
		{
			err -= y;
			x1 += x1 < p2->cx ? 1 : -1;
		}
		if (e2 < y)
		{
			err += x;
			y1 += y1 < p2->cy ? 1 : -1;
		}
	}
	pixel_put_to_image(0xFFFFFF, e, x1, y1);
}

void			pixel_put_to_image(int color, t_env *e, int x, int y)
{
	if (x > 0 && y > 0 && x < MIN_WIDTH && y < MIN_HEIGHT)
	{
		e->data[y * e->size + x * e->bpp / 8] = (char)color;
		e->data[y * e->size + x * e->bpp / 8 + 1] = (char)color;
		e->data[y * e->size + x * e->bpp / 8 + 2] = (char)color;
	}
}

t_matrix		*calc_xy(t_env *e, t_matrix *m)
{
	int			z;

	z = (e->scale * m->z) / 10;
	m->cx = e->left + e->scale * m->x + e->scale * m->y;
	m->cy = e->top + (e->scale / 2) * m->x - (e->scale / 2) * m->y - z;
	return (m);
}

int				add_points(t_env *e)
{
	int			chck;
	t_matrix	*m;
	t_matrix	*a;

	m = e->m->prev;
	chck = norminator(e, m);
	while ((m = m->next) != e->m || chck == 0)
	{
		m = calc_xy(e, m);
		if ((chck = 1) && m != e->m)
		{
			if (m->prev->x == m->x)
				draw_line(e, m->cx, m->cy, m->prev);
			a = m->prev;
			while (a != e->m && a->x >= m->x - 1)
			{
				if (a->y == m->y)
					draw_line(e, m->cx, m->cy, a);
				a = a->prev;
			}
			if (a == e->m && a->x >= m->x - 1 && a->y == m->y)
				draw_line(e, m->cx, m->cy, a);
		}
	}
	return (0);
}

void			draw(t_matrix *m, const int fd)
{
	t_env		e;

	m = m->prev;
	e.scale = m->x > 200 || m->y > 200 ? 0.5 : 20;
	e.left = 100;
	e.top = 400;
	e.mlx = mlx_init();
	e.win = mlx_new_window(e.mlx, MAX_WIDTH, MAX_HEIGHT, "FdF");
	e.img = mlx_new_image(e.mlx, MIN_WIDTH, MIN_HEIGHT);
	e.data = mlx_get_data_addr(e.img, &e.bpp, &e.size, &e.endian);
	e.m = m->next;
	add_points(&e);
	legends(&e);
	mlx_put_image_to_window(e.mlx, e.win, e.img, 0, 0);
	mlx_key_hook(e.win, key_actions, &e);
	mlx_loop(e.mlx);
	close(fd);
}
