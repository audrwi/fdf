/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 15:31:14 by aboucher          #+#    #+#             */
/*   Updated: 2016/01/22 16:12:00 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "libft/libft.h"
# include <mlx.h>
# include <stdio.h>

# define MIN_WIDTH 800
# define MAX_WIDTH 800
# define MIN_HEIGHT 600
# define MAX_HEIGHT 700

typedef struct		s_matrix
{
	int				x;
	int				y;
	int				z;
	int				orig;
	int				cx;
	int				cy;
	struct s_matrix	*prev;
	struct s_matrix	*next;
}					t_matrix;

typedef struct		s_env
{
	void			*mlx;
	void			*win;
	void			*img;
	char			*data;
	int				bpp;
	int				size;
	int				endian;
	float			scale;
	int				left;
	int				top;
	t_matrix		*m;
}					t_env;

int					print_error(char *error);
t_matrix			*new_bloc(t_matrix *p);
t_matrix			*fill_matrix(int x, int y, char **tab, t_matrix *m);
int					contains_numbers(char *str);
int					key_actions(int keycode, t_env *e);
void				draw_line(t_env *e, int x1, int y1, t_matrix *p2);
void				pixel_put_to_image(int color, t_env *e, int x, int y);
t_matrix			*calc_xy(t_env *e, t_matrix *m);
int					add_points(t_env *e);
void				draw(t_matrix *m, const int fd);
void				zoom(int keycode, t_env *e);
void				move(int keycode, t_env *e);
void				change_z(int keycode, t_env *e);
void				init(t_env *e);
void				new_image(t_env *e);
void				legends(t_env *e);
void				key_legend(t_env *e, int c);
void				print_key(t_env *e, char *str, int x, int y);
int					norminator(t_env *e, t_matrix *m);

#endif
