/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 13:26:06 by aboucher          #+#    #+#             */
/*   Updated: 2015/12/04 11:37:18 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	size_t	i;
	char	*s1;
	char	*s2;
	char	*cpy;

	i = 0;
	s1 = (char *)src;
	s2 = (char *)dst;
	cpy = (char *)malloc(sizeof(char) * len);
	if (cpy == NULL)
		return (NULL);
	while (i < len)
	{
		cpy[i] = s1[i];
		i++;
	}
	i = 0;
	while (i < len)
	{
		s2[i] = cpy[i];
		i++;
	}
	return (s2);
}
