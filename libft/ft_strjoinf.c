/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoinf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/03 16:35:12 by aboucher          #+#    #+#             */
/*   Updated: 2016/01/03 16:38:45 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoinf(char *s1, char *s2, int f)
{
	int		i;
	int		j;
	char	*res;

	i = -1;
	j = -1;
	res = (char *)malloc(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2) + 1));
	if (!res)
		return (NULL);
	while (s1[++i])
		res[i] = s1[i];
	if (f == 1 || f > 2)
		free(s1);
	while (s2[++j])
	{
		res[i] = s2[j];
		i++;
	}
	if (f >= 2)
		free(s2);
	res[i] = '\0';
	return (res);
}
