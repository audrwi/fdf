/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 11:34:02 by aboucher          #+#    #+#             */
/*   Updated: 2015/12/03 18:10:19 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	size_t	i;
	size_t	j;
	size_t	st;
	char	*str1;
	char	*str2;

	i = 0;
	j = (size_t)start;
	st = j;
	str1 = (char *)malloc(sizeof(char) * (len + 1));
	str2 = (char *)s;
	if (!str2)
		return (NULL);
	if (str1 == NULL)
		return (NULL);
	while (j < (st + len))
	{
		str1[i] = str2[j];
		i++;
		j++;
	}
	str1[i] = '\0';
	return (str1);
}
