/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 15:36:26 by aboucher          #+#    #+#             */
/*   Updated: 2016/01/22 15:59:58 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_matrix		*new_bloc(t_matrix *p)
{
	t_matrix	*new;

	if (!(new = (t_matrix *)malloc(sizeof(t_matrix))))
		return (NULL);
	new->x = 0;
	new->y = 0;
	new->z = 0;
	new->orig = 0;
	new->cx = 0;
	new->cy = 0;
	new->prev = p;
	new->next = NULL;
	return (new);
}

t_matrix		*fill_matrix(int x, int y, char **tab, t_matrix *m)
{
	t_matrix	*rslt;

	if (!tab || !tab[0])
		return (m);
	if ((rslt = m) && m->next)
	{
		while (m->next && !(m->next->x == 0 && m->next->y == 0))
			m = m->next;
		m->next = new_bloc(m);
		m = m->next;
	}
	while (tab[++y])
	{
		m->x = x;
		m->y = y;
		if (!(m->z = ft_atoi(tab[y])) && ft_strcmp(tab[y], "0"))
			return (NULL);
		m->orig = ft_atoi(tab[y]);
		m->next = tab[y + 1] ? new_bloc(m) : rslt;
		m = m->next->next ? m : m->next;
		free(tab[y]);
	}
	free(tab);
	m->next->prev = tab[1] ? m : rslt;
	return (rslt);
}

int				contains_numbers(char *str)
{
	int			i;

	i = 0;
	while (str[i])
	{
		if (ft_isdigit(str[i]))
			return (1);
		i++;
	}
	return (0);
}

int				print_error(char *error)
{
	ft_putstr_fd("\033[0;31merror\033[0m: ", 2);
	ft_putendl_fd(error, 2);
	return (0);
}

int				main(int ac, char **av)
{
	int			x;
	int			fd;
	int			rslt;
	char		*line;
	t_matrix	*m;

	if (ac != 2)
		return (print_error("one argument required"));
	if ((fd = open(av[1], O_RDONLY)) == -1)
		return (print_error("no such file or directory"));
	x = 0;
	while ((rslt = get_next_line(fd, &line)) > 0)
	{
		if (!m && contains_numbers(line))
			m = new_bloc(NULL);
		if (m && !(m = fill_matrix(x++, -1, ft_strsplit(line, ' '), m)))
			return (print_error("invalid map"));
		free(line);
	}
	if (rslt == -1 || !m)
		return (print_error("empty map or something went wrong"));
	draw(m, fd);
	return (0);
}
